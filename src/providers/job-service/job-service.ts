import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the JobServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class JobServiceProvider {

  constructor(public http: Http) {

  }

  getAllJobs() {
  	var headers = new Headers();
  	headers.append('Access-Control-Allow-Origin' , '*');
  	headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  	headers.append('Accept','application/json');
  	headers.append('content-type','application/json');
  	let options = new RequestOptions({withCredentials: true});

  	var results = this.http.get('http://138.197.132.1/api/job', options);
  	return results;
  }

  getJob(jobId) {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    let options = new RequestOptions({withCredentials: true});

    var results = this.http.get('http://138.197.132.1/api/job/' + jobId, options);
    return results;
  }

  getJobOffers(userId) {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    let options = new RequestOptions({withCredentials: true});

    var results = this.http.get('http://138.197.132.1/api/job/', options);
    return results;
  }

}
