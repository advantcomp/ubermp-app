import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JobSettingsPage } from './job-settings';

@NgModule({
  declarations: [
    JobSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(JobSettingsPage),
  ],
})
export class JobSettingsPageModule {}