import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController } from 'ionic-angular';
// import { Http } from '@angular/http';

// import { TranslateService } from '@ngx-translate/core';
import { DataFinder } from '../../providers/datafinder';
// import { ItemApi } from '../../providers/item-api';

import { JobDetailsPage } from '../job-details/job-details';

@IonicPage()
@Component({
  selector: 'page-job-settings',
  templateUrl: 'job-settings.html',
})
export class JobSettingsPage {

  constructor(private viewCtrl: ViewController, public navCtrl: NavController, private dataFinder: DataFinder) {

  }

  ionViewDidLoad() {

  }
  
  closeJobSettings() {
    this.viewCtrl.dismiss();
  }
}
