import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LogHoursPage } from '../log-hours/log-hours';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'



@Component({
  selector: 'page-work',
  templateUrl: 'work-logs.html'
})
export class WorkPage {
  jobitems: Array<{title: string}>;
  posts: any;

  logHoursPage = LogHoursPage;
  months: any = [];

  items: any = [];
  itemExpandHeight: number = 100;

  constructor(public navCtrl: NavController, public http: Http) {

    this.items = [
      { expanded: false },
      { expanded: false },
      { expanded: false },
      { expanded: false }
  ], 
  this.months = [
    { month: 'January', hours: 41, image: '/assets/img/thumbnail_january.jpg' },
    { month: 'February', hours: 20, image: '/assets/img/thumbnail_february.jpg'},
    { month: 'March', hours: 10, image: '/assets/img/thumbnail_march.jpg'},
    { month: 'April', hours: 5, image: '/assets/img/thumbnail_april.jpg'},
    { month: 'May', hours: 40, image: '/assets/img/thumbnail_may.jpg'},
    { month: 'June', hours: 15, image: '/assets/img/thumbnail_june.jpg'},
    { month: 'July', hours: 22, image: '/assets/img/thumbnail_july.jpg'},
    { month: 'August', hours: 13, image: '/assets/img/thumbnail_august.jpg'},
    { month: 'September', hours: 5, image: '/assets/img/thumbnail_september.jpg'},
    { month: 'October', hours: 20, image: '/assets/img/thumbnail_october.jpg'},
    { month: 'November', hours: 35, image: '/assets/img/thumbnail_november.jpg'},
    { month: 'December', hours: 15, image: '/assets/img/thumbnail_december.jpg'}
  ];
  }

  expandItem(item){
    if(item.expanded === true){
      item.expanded = false;
    }else {
      item.expanded = true;
    }
    
  }

  launchPage(){
    this.navCtrl.push(LogHoursPage);
  }

}
