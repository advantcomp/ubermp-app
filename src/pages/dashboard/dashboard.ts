import { Component, ViewChild } from '@angular/core';
import {  NavController} from 'ionic-angular';
import { Geolocation, GeolocationOptions ,Geoposition ,PositionError  } from '@ionic-native/geolocation';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { JobServiceProvider } from '../../providers/job-service/job-service';

//pages 
import { ModalPage } from '../modal/modal';
import { NotificationsPage } from '../notifications/notifications';
import { JobSettingsPage } from '../job-settings/job-settings';
import { WorkPage } from '../work-logs/work-logs';
import { JobDetailsPage } from '../job-details/job-details';

//install the actual types npm install @types/google-maps --save
declare var google;

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})

export class DashboardPage {
  options : GeolocationOptions;
  currentPos : any;

  @ViewChild('map') mapContainer;
  map: any;
  infoWindows: any;
  jobMarkers: any;
  userMarkers: any;
  directionsDisplay: any;
  directionsService: any;
  jobs: any;

  constructor(public navCtrl: NavController, public http: Http, public geolocation: Geolocation, private jobServiceProvider: JobServiceProvider) {
    this.infoWindows = [];
    this.jobMarkers = [];
    this.userMarkers = [];
  }

  ionViewWillEnter() {
    this.displayGoogleMap();
    //this.getUserLocation();
    this.getJobLocations();
    //this.getMarkers();
  }

  displayGoogleMap() {
  
      this.directionsService = new google.maps.DirectionsService();
      this.directionsDisplay = new google.maps.DirectionsRenderer();

      let latLng = new google.maps.LatLng(46.920255, -123.002929);
      this.currentPos = latLng;

      let mapOptions = {
        center: latLng,
        disableDefaultUI: true,
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 33
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [
            {
                "saturation": "-100",
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text",
        "stylers": [
            {
                "gamma": "0.75",
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "lightness": "-37"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f9f9f9"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "lightness": "40"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "lightness": "-37"
            },
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "lightness": "100"
            },
            {
                "weight": "2"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "labels.icon",
        "stylers": [
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "lightness": "80"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "lightness": "0"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": "-4"
            },
            {
                "saturation": "-100"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c5dac6"
            },
            {
                "visibility": "on"
            },
            {
                "saturation": "-95"
            },
            {
                "lightness": "62"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 20
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text",
        "stylers": [
            {
                "gamma": "0.50"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "gamma": "0.50"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c5c6c6"
            },
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "lightness": "-13"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.icon",
        "stylers": [
            {
                "lightness": "0"
            },
            {
                "gamma": "1.09"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e4d7c6"
            },
            {
                "saturation": "-100"
            },
            {
                "lightness": "47"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "lightness": "-12"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#fbfaf7"
            },
            {
                "lightness": "77"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "lightness": "-5"
            },
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "lightness": "-15"
            }
        ]
    },
    {
        "featureType": "transit.station.airport",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": "47"
            },
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#acbcc9"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "saturation": "53"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "lightness": "-42"
            },
            {
                "saturation": "17"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "lightness": "61"
            }
        ]
    }
]
      }
      this.map = new google.maps.Map(this.mapContainer.nativeElement, mapOptions);

      this.map.addListener('click', () => {
        for(let window of this.infoWindows) {
          window.close();
        }
      });
    
  }

  getUserLocation(){
    this.options = {
      enableHighAccuracy : true
    };

    this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {

      this.currentPos = {
        lat: pos.coords.latitude,
        lng: pos.coords.longitude
      };
      console.log(this.currentPos);

      // Create users marker
      var userMarker = new google.maps.Marker({
        position: new google.maps.LatLng(this.currentPos.lat, this.currentPos.lng),
        title: 'Current Location',
        map: this.map,
      });

      this.userMarkers.push(userMarker);

      // Center map on location
      this.map.setCenter(this.currentPos);

    },(err : PositionError)=>{
      console.log("error : " + err.message);
    });
  }

  getJobLocations(){
    this.jobServiceProvider.getAllJobs().subscribe(
      data => {
        this.jobs = JSON.parse(data["_body"]).jobs;
        console.log(this.jobs);
        this.loadJobMarkers();
      },
      err => {
          console.log(err);
      },
      () => console.log('Job Locations Loaded!')
    );
  }

  loadJobMarkers(){
    var markerId = 0;

    for(let job of this.jobs) {

      // Create job marker
      var jobMarker = new google.maps.Marker({
        position: new google.maps.LatLng(job.lat, job.long),
        title: job.title,
        start: job.starttime,
        end: job.endtime,
        description: job.description,
        map: this.map,
        jobId: job.id,
        markerId: markerId,
      });

      this.jobMarkers.push(jobMarker);
      // Create infowindow
      /* console.log(this.infoWindows);
      this.infoWindows[job.id] = new google.maps.InfoWindow();
      console.log(this.infoWindows[job.id]);
      google.maps.event.addListener(this.jobMarker, 'click', function() {
        this.infoWindows[this.get('jobId')].setContent(this.get('title'));
        this.infoWindows[this.get('jobId')].open(this.map, this);
      }); */
      this.addInfoWindowToMarker(jobMarker);

      markerId++;
    }
  }

  getMarkers() {
    this.http.get('assets/data/markers.json')
    .map((res) => res.json())
    .subscribe(data => {
      this.addMarkersToMap(data);
    });
  }
  

  addMarkersToMap(markers) {
    var i = 0;
    for(let marker of markers) {
      var position = new google.maps.LatLng(marker.latitude, marker.longitude);
      var dogwalkMarker = new google.maps.Marker({
        position: position,
        title: marker.name,
        start: marker.start,
        end: marker.end,
        jobId: i,
        description: marker.description,
        icon: 'assets/img/marker.png'});
      dogwalkMarker.setMap(this.map);
      this.addInfoWindowToMarker(dogwalkMarker);
      i++;
    }
  }

  //add InfoWindow to markers
  addInfoWindowToMarker(marker) {
    /* var infoWindowContent = 
    '<div id="iw-container">' +
      '<div class="iw-title">' + marker.title+ '</div>' +
      '<div class="iw-content">' +
        '<div class="iw-subTitle"> Start Date </div>' +
        '<p>' + marker.start + '</p>' +
        '<div class="iw-subTitle">End Date</div>' +
        '<p>' + marker.end + '</p>'+
        '<div class="iw-subTitle">Description</div>' +
        '<p class="description">' + marker.description + '</p>'+
        '<div class="options">' +
        '<div (click)="openJobDescription();" class="check"><p></p></div>' +
        '</div>' + 
      '</div>' +
      '<div class="iw-bottom-gradient"></div>' +
    '</div>',

    maxWidth: 350 */

    var jobPreviewContainer = document.querySelector('#job-preview-container');
    var infoWindow = new google.maps.InfoWindow({
      content: 
      '<div class="iw-title">' + marker.title + '</div>'
    });
    marker.addListener('click', () => {
      this.closeAllInfoWindows();
      this.changeJobPreviewContent(marker);
      infoWindow.open(this.map, marker);
      jobPreviewContainer.classList.add("active");
      console.log(marker.position);
      this.map.panTo(marker.position);
    });
    google.maps.event.addListener(infoWindow,'closeclick',function(){
      jobPreviewContainer.classList.remove("active"); 
    });

    this.infoWindows.push(infoWindow);

    // Event that closes the Info Window with a click on the map
    this.map.addListener('click', (event) => {
      this.closeAllInfoWindows();
      jobPreviewContainer.classList.remove("active"); 
    });

    // *
    // START INFOWINDOW CUSTOMIZE.
    // The google.maps.event.addListener() event expects
    // the creation of the infowindow HTML structure 'domready'
    // and before the opening of the infowindow, defined styles are applied.
    // *
    google.maps.event.addListener(infoWindow, 'domready', function() {

      // CUSTOMIZE INFOWINDOWS

      // Reference to the DIV that wraps the bottom of infowindow
      var iwOuter = document.querySelector('.gm-style-iw');
      iwOuter.classList.add('fade-in');
      console.log(iwOuter);

      /* Since this div is in a position prior to .gm-div style-iw.
       * We use jQuery and create a iwBackground variable,
       * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
      */
      var iwBackground = document.getElementsByClassName('gm-style-iw')[0];
      // Removes background shadow DIV
      // iwBackground.children[0].style.display = 'none';

      // Removes white background DIV
      (iwBackground.children[1] as HTMLElement).style.display = 'none';

      // Old infowindow
      (iwBackground.parentNode.childNodes[0] as HTMLElement).style.display = 'none';

      // Infowindow X button
      (iwBackground.children[1] as HTMLElement).style.display = 'none';

      (iwBackground.parentNode.childNodes[2] as HTMLElement).style.right = '15px';
      (iwBackground.parentNode.childNodes[2] as HTMLElement).style.top = '9px';

      // Moves the infowindow 115px to the right.
      (iwOuter.parentNode.parentNode as HTMLElement).style.top = '20px';

      // Changing the infowindow close button X
      (((iwOuter.parentNode as HTMLElement).children[2] as HTMLElement).children[0] as HTMLElement).style.display = 'none';
      ((iwOuter.parentNode as HTMLElement).children[2] as HTMLElement).innerHTML = '<div class="x-icon infowindow-x" name="close" style=""><div></div></div>';

      // Moves the shadow of the arrow 76px to the left margin.
      //iwBackground.children[0].attr('style', function(i,s){ return s + 'left: 76px !important;'});

      // Moves the arrow 76px to the left margin.
      //iwBackground.children[2].attr('style', function(i,s){ return s + 'left: 76px !important;'});

      // Changes the desired tail shadow color.
      //iwBackground.children[2].find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

      // Reference to the div that groups the close button elements.
      //var iwCloseBtn = iwOuter.next();

      // Apply the desired effect to the close button
      //iwCloseBtn.css({opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

      // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
      /*if($('.iw-content').height() < 140){
        document.querySelector('.iw-bottom-gradient').css({display: 'none'});
      }*/

      // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
      /*iwCloseBtn.mouseout(function(){
        document.querySelector(this).css({opacity: '1'});
      });*/
    });
  }

  changeJobPreviewContent(marker) {
    // Set job preview content
    var jobPreviewHeading = document.querySelector('#job-preview-heading');
    jobPreviewHeading.innerHTML = marker.title;
    var jobPreviewDescription = document.querySelector('#job-preview-description');
    jobPreviewDescription.innerHTML = marker.description;

    // Set the data attributes for buttons
    var btnNavigate = document.querySelector('#btn-navigate'); 
    btnNavigate.setAttribute('data-marker-id', marker.markerId); 
    var btnJobDetails = document.querySelector('#btn-details'); 
    btnJobDetails.setAttribute('data-job-id', marker.jobId); 

    // Fade ins
    var jobPreviewHeader = document.querySelector('#job-preview-header');
    jobPreviewHeader.classList.remove('fade-in');
    jobPreviewHeader.classList.add('fade-in');
    var jobPreviewContent = document.querySelector('#job-preview-content');
    jobPreviewContent.classList.remove('fade-in');
    jobPreviewContent.classList.add('fade-in');
  }

  
  closeAllInfoWindows() {
    for(let window of this.infoWindows) {
      window.close();
    }
  }
  

  openModal() {
    this.navCtrl.push(ModalPage);
  }

  openNotifications() {
    this.navCtrl.push(NotificationsPage);
  }

  openJobSettings() {
    this.navCtrl.push(JobSettingsPage);
  }

  navigateToJob() {
    this.closeJobPreview();
    this.closeAllInfoWindows();

    var btnNavigate = document.querySelector('#btn-navigate'); 
    var markerId = btnNavigate.getAttribute('data-marker-id');
    var jobMarker = this.jobMarkers[markerId];
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.directionsDisplay.setMap(this.map);
    console.log(this.directionsDisplay);

    var directionsDisplay = this.directionsDisplay;

    var request = {
      origin: this.currentPos,
      destination: jobMarker.position,
      travelMode: 'DRIVING'
    };
    this.directionsService.route(request, function(result, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(result);
      }
    });
  }

  closeJobPreview() {
    var jobPreviewContainer = document.querySelector('#job-preview-container');
    jobPreviewContainer.classList.remove("active");  
  }

  openWorkLogs() {
    this.navCtrl.push(WorkPage);
  }

  openJobDetails() {
    var btnJobDetails = document.querySelector('#btn-details'); 
    var jobId = btnJobDetails.getAttribute('data-job-id');
    this.navCtrl.push(JobDetailsPage, {
      jobId: jobId
    });
  }
}


