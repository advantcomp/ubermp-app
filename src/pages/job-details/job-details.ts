import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { JobServiceProvider } from '../../providers/job-service/job-service';

declare var google;

@IonicPage()
@Component({
  selector: 'page-job-details',
  templateUrl: 'job-details.html',
})
export class JobDetailsPage {

  @ViewChild('map')  mapElement;
  map: any;
  public jobId;
  public job = [];


  constructor(public navCtrl: NavController, public navParams: NavParams, private jobServiceProvider: JobServiceProvider) {
    let jobId = navParams.get('jobId');
    this.jobServiceProvider.getJob(jobId).subscribe(
      data => {
        this.job = JSON.parse(data["_body"]).jobs;
        console.log(this.job);
      },
      err => {
          console.log(err);
      },
      () => console.log('Job Locations Loaded!')
    );
  }

  getJobDetails(jobId){
    this.jobServiceProvider.getJob(jobId).subscribe(
      data => {
        this.job = JSON.parse(data["_body"]).jobs;
        console.log(this.job);
      },
      err => {
          console.log(err);
      },
      () => console.log('Job Locations Loaded!')
    );
  }

  ionViewDidLoad(){
    this.initMap();
  }

  initMap(){

    let latLng = new google.maps.LatLng(41.758674,-87.58482);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

  }

  goBack(){
    this.navCtrl.pop();
  }

}
